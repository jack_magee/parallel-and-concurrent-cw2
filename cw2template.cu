#include <iostream>
#include <stdlib.h>

using namespace std;

struct Image {
    int width;
    int height;
    int bytes;
    unsigned char *img;
    unsigned char *dev_img;
};
  
//shared memory convolution
__global__ void sharedMemoryConvolution(unsigned char* inputImage, unsigned char* outputImage, const float* kernel, int width, int height,const int kerCols,const int kerRows)
{
  const int imgBlockSize = 15+3; // for sobel

  //const int imgBlockSize = 15+5; // for standard

  __shared__ float imgBlock[imgBlockSize][imgBlockSize]; //a block of the image in shared memory spanning the block size plus hangover

  int tileSize = 16;

  int kerRowsRadius = kerRows/2; //hangover the sides and top of image to keep image centralized.
  int kerColsRadius = kerCols/2;

  for (int k = 0; k < 3; k++) {
    int currPos = threadIdx.y * tileSize + threadIdx.x;
    int currPosY = currPos/imgBlockSize; //current position in the row
    int currPosX = currPos%imgBlockSize; //current position in the col

    int inputImageY = blockIdx.y *tileSize + currPosY - kerColsRadius; //respective col location in the input image
    int inputImageX = blockIdx.x *tileSize + currPosX - kerRowsRadius; //respective row location in the input image
    int inputImageLocation = (inputImageY * width +inputImageX) * 3 + k;   //respective location in the input image
    if(inputImageY >= 0 && //make sure current location is actually within the input image
       inputImageX >= 0 &&
       inputImageY < height &&
       inputImageX < width){
        imgBlock[currPosY][currPosX] = inputImage[inputImageLocation];  //copy value from image into shared memory
      }
      else{
        imgBlock[currPosY][currPosX] = 0; //if not in image then add padding of 0;
      }
      

    currPos += tileSize * tileSize; //move along a tile to cover all values
    currPosY = currPos/imgBlockSize;
    currPosX = currPos%imgBlockSize;
    inputImageY = blockIdx.y *tileSize + currPosY - kerColsRadius;
    inputImageX = blockIdx.x *tileSize + currPosX - kerRowsRadius;
    inputImageLocation = (inputImageY *width +inputImageX) * 3 + k;
    if(currPosY < imgBlockSize && currPosX < imgBlockSize){ //make sure moved position is still within the block
      if(inputImageY >= 0 && //make sure current location is actually within the input image
         inputImageX >=0 &&
         inputImageY < height &&  
         inputImageX < width){
           imgBlock[currPosY][currPosX] = inputImage[inputImageLocation]; //copy value from image into shared memory
         }
        else{
          imgBlock[currPosY][currPosX] = 0;
        }
   }

   //once everything hsa been added to the shared memory need to sync threads so that nothing is getting left behind
   __syncthreads();


 		//compute kernel convolution
 		float temp = 0;
 		for (int x = 0; x < kerRows; x++){ //go through the kernel and do calculation on mini image stored in shared memory
      for(int y = 0; y < kerCols; y++){
        temp += imgBlock[threadIdx.y + y][threadIdx.x + x] * kernel[y * kerCols + x]; //do kernel calculation on small image segment stored in shared mem
      }
    }
 		
 		int y = blockIdx.y * tileSize + threadIdx.y;
 		int x = blockIdx.x * tileSize + threadIdx.x;
    if(y < height && x < width){ //make sure calcualted value is in the image range
       outputImage[(y * width + x) * 3 + k] = temp; //update the output image
    }
    //once calculated for each k, sync the threads so can move onto the next channel
     __syncthreads();
  }
}

//constant memory 2d convolution
__global__ void constantMemoryConvolution(unsigned char* inputImage, unsigned char* outputImage, const float* kernel, int width, int height, int kerCols, int kerRows)
{    
  float temp;
  int row = threadIdx.y + blockIdx.y * blockDim.y;   //row index
  int col = threadIdx.x + blockIdx.x * blockDim.x;   //col index
  
  int kerRowsRadius = kerRows/2; //hangover the sides and top of image to keep image centralized.
  int kerColsRadius = kerCols/2;
  
  for (int k = 0; k < 3; k++){      //cycle on rgb
    if(row < height && col < width ){ // if the current value is within the image
      temp = 0; //temp value to increment for each value calculated
      int startRow = row - kerRowsRadius;  //start value hungover respective side of image
      int startCol = col - kerColsRadius;  //start value hungover respective side of image
  
      for(int i = 0; i < kerRows; i++){ //go throught the ker rows
        for(int j = 0; j < kerCols; j++){ //go through the ker columns
  
          int currRow = startRow + i; //current row to be working on
          int currCol = startCol + j; //current col to be working on
  
          if(currRow >= 0 && //check to see if the current position is within the image
            currCol >= 0 &&  
            currRow < height &&
            currCol < width){

            temp += inputImage[(currRow * width + currCol )*3 + k] * kernel[i * kerRows + j]; //increment temp by value at current location * kernel value over that current location
          }
          else{
            temp = 0; //if the value is out of the image range then temp = 0. this would where any padding would occur
          } 
        }
      }    
    outputImage[(row* width + col) * 3 + k] = (unsigned char)temp; //put the value in the output image in the right location
    }
  }
}


// Reads a color PPM image file (name provided), and
// saves data in the provided Image structure. 
// The max_col_val is set to the value read from the 
// input file. This is used later for writing output image. 
int readInpImg (const char * fname, Image & source, int & max_col_val){

    FILE *inputImageLocation;
  
    if (!(inputImageLocation = fopen(fname, "rb")))
    {
        printf("Couldn't open file %s for reading.\n", fname);
        return 1;
    }
  
    char p,s;
    fscanf(inputImageLocation, "%c%c\n", &p, &s);
    if (p != 'P' || s != '6')   // Is it a valid format?
    {
        printf("Not a valid PPM file (%c %c)\n", p, s);
        exit(1);
    }
  
    fscanf(inputImageLocation, "%d %d\n", &source.width, &source.height);  
    fscanf(inputImageLocation, "%d\n", &max_col_val);
  
    int pixels = source.width * source.height;
    source.bytes = pixels * 3;  // 3 => colored image with r, g, and b 3 
    source.img = (unsigned char*)malloc(source.bytes);
    if (fread(source.img, sizeof(unsigned char), source.bytes, inputImageLocation) != source.bytes)
      {
         printf("Error reading file.\n");
         exit(1);
      }
    fclose(inputImageLocation);
    return 0;
  }
  
// Write a color image into a file (name provided) using PPM file format.  
// Image structure represents the image in the memory. 
int writeOutImg(const char * fname, const Image & roted, const int max_col_val){
 
  FILE *out;
  if (!(out = fopen(fname, "wb")))
  {
    printf("Couldn't open file for output.\n");
    return 1;
  }
  fprintf(out, "P6\n%d %d\n%d\n", roted.width, roted.height, max_col_val);
  if (fwrite(roted.img, sizeof(unsigned char), roted.bytes , out) != roted.bytes)
  {
    printf("Error writing file.\n");
    return 1;
  }
  fclose(out);
  return 0;
}

//main function used for setting everything up and calling the convolution
int main(int argc, char **argv){

  
  if (argc != 3){
    printf("Usage: exec filename\n");
    exit(1);
  }
  char *fname = argv[1];

  //Read the input file
  Image source;

  int max_col_val;
  if (readInpImg(fname, source, max_col_val) != 0)  exit(1);

	int imageHeight = source.height;  //get image height
  int imageWidth = source.width;    //get image width
  int imageSize = source.bytes;     //get image size
	unsigned char* hostinputImage = source.img; //pointer to image data
	unsigned char* hostOutputImage = (unsigned char*)malloc(imageSize); //allocated memory for output image to be stored
	unsigned char* deviceinputImage;  //pointer for the device image data
	unsigned char* deviceOutputImage; //pointer for the device output image
  float* deviceKernel;  //pointer for the device kernel getting used
  
  /*Below are kernels that can be used. To use comment out the one wanting to use.*/

  /*
  //Standard bluring filter
  int kerCols = 5; //for the standard blur
  int kerRows = 5; //for the standard blur
	float hostKernel[kerRows * kerCols]={
		0.04, 0.04, 0.04, 0.04, 0.04,
		0.04, 0.04, 0.04, 0.04, 0.04,
		0.04, 0.04, 0.04, 0.04, 0.04,
		0.04, 0.04, 0.04, 0.04, 0.04,
		0.04, 0.04, 0.04, 0.04, 0.04
  };
  */

  //edge detection sobel fitler
  int kerCols = 3; //for the sobel filter
  int kerRows = 3; //for the sobel filter
  float hostKernel[kerRows * kerCols] = {
    -1, -2, -1,
    0, 0, 0,
    1, 2, 1
  };

  

  cudaDeviceReset();
  cudaMalloc((void **) &deviceinputImage, imageSize); //allocate space on card for input image
  cudaMalloc((void **) &deviceOutputImage, imageSize); //and space to store output image
  cudaMalloc((void **) &deviceKernel, kerRows * kerCols * sizeof(float)); //and space for the kernel
  cudaMemcpy(deviceinputImage, hostinputImage, imageSize, cudaMemcpyHostToDevice); //copy input image to the card
  cudaMemcpy(deviceKernel, hostKernel, kerRows * kerCols * sizeof(float), cudaMemcpyHostToDevice); //and the kernel to the card

	dim3 dimGrid(ceil((float) imageWidth/16), ceil((float) imageHeight/16)); //split the grid into equal sized blocks, making sure not to over or underallocate
  dim3 dimBlock(16,16); //needs only be 2-d
  
  cudaEvent_t start, stop;
  float time;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);
  cudaEventRecord(start, 0);

  if(*argv[2] == 'c'){
    constantMemoryConvolution<<<dimGrid,dimBlock>>>(deviceinputImage, deviceOutputImage, deviceKernel, imageWidth, imageHeight, kerCols, kerRows); //call the convolution
  }
  else if(*argv[2] == 's'){
    sharedMemoryConvolution<<<dimGrid,dimBlock>>>(deviceinputImage, deviceOutputImage, deviceKernel, imageWidth, imageHeight, kerCols, kerRows); //call the convolution
  }
  else{
    printf("Please retry with c for constant, or s for shared.\n");
    exit(1);
  }
  cudaMemcpy(hostOutputImage, deviceOutputImage, imageSize, cudaMemcpyDeviceToHost); //copy the final image back to the host
  cudaFree((void *) &deviceinputImage); //free up the memory on the device
  cudaFree((void *) &deviceOutputImage);
  cudaFree((void *) &deviceKernel);

  cudaEventRecord(stop, 0);
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&time, start, stop);

  cout << "Time Taken: " << time << " milliseconds"<< endl;

  //generate a final image with all details ready to output.
  Image output = {
    imageWidth,
    imageHeight,
    imageSize,
    hostOutputImage,
    nullptr,
  };
    
  if (writeOutImg("med_const_sobel.ppm", output, max_col_val) != 0) //Output the new filtered image
  exit(1);

  free(source.img); //free up memory on the host
  free(hostOutputImage);
  return 0;
}
/*Apologies for the late submission of the cw.
My Ubuntu install on my laptop corrupted and ended up losing some of the work.
Needed to set up on PC and then redo work.*/